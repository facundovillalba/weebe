# weebe

Steps to Setup

1. Clone the repository

    git clone https://gitlab.com/facundovillalba/weebe.git

2. Deploy the app using maven wrapper

    cd weebe

    ./mvnw appengine:deploy

Usage

The service provides endpoints for:

1. Add device:/device

    HTTP method post and require a valid device on body.
2. List devices: /device
3. List device details for specific id o MAC: /device/id/{id} and /device/mac/{mac}
    