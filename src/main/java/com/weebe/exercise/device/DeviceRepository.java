package com.weebe.exercise.device;

import org.springframework.stereotype.Repository;

import com.google.cloud.datastore.Datastore;
import com.google.cloud.datastore.DatastoreOptions;
import com.google.cloud.datastore.Entity;
import com.google.cloud.datastore.Key;

//@Repository
public class DeviceRepository {
	// weebe-233718
	public static final String DEVICE_KIND = "Device";
	public static final String DEVICE_PROPERTY_MAC = "macAddress";
	public static final String DEVICE_PROPERTY_TIME = "timestamp";

	Datastore datastore = DatastoreOptions.getDefaultInstance().getService();

	public void saveDevice(Device device) {
		String name = device.getMacAddress();
		Key deviceKey = datastore.newKeyFactory().setKind(DEVICE_KIND).newKey(name);
		Entity deviceEntity = Entity.newBuilder(deviceKey).set(DEVICE_PROPERTY_MAC, device.getMacAddress())
				.set(DEVICE_PROPERTY_TIME, device.getTimestamp()).build();
		datastore.put(deviceEntity);
	}

}