package com.weebe.exercise.device;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DeviceService {

	@Autowired
	private GoogleDeviceRepository deviceRepository;

	public void addDevice(Device device) throws InvalidMACException, BadTimestampException {
		if (!validateMacAddress(device)) {
			throw new InvalidMACException("MAC Address format must be FF:FF:FF:FF:FF:FF");
		}
		if (!validateTimestamp(device)) {
			throw new BadTimestampException("Timestamp must be at least 01/01/2018");
		}
		Optional<List<Device>> deviceOpt = deviceRepository.findByMacAddress(device.getMacAddress());
		if (!deviceOpt.isPresent()) {
			deviceRepository.save(device);
		} else {
			device.setId(deviceOpt.get().get(0).getId());
			deviceRepository.save(device);
		}
	}

	public List<Device> getAllDevices() {
		List<Device> devices = new ArrayList<>();
		deviceRepository.findAll().forEach((device) -> devices.add(device));
		return devices;
	}

	private boolean validateTimestamp(Device device) {
		String limitDate = "31/12/2017";
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		LocalDate deviceTimestamp = LocalDate.parse(device.getTimestamp(), formatter);
		LocalDate limitTimestamp = LocalDate.parse(limitDate, formatter);
		return deviceTimestamp.isAfter(limitTimestamp);

	}

	private boolean validateMacAddress(Device device) {
		String macRegex = "^([0-9A-F]{2}[:-]){5}([0-9A-F]{2})$";
		Pattern p = Pattern.compile(macRegex);
		Matcher m = p.matcher(device.getMacAddress());
		return m.find();
	}

	public Device getDeviceById(String id) throws DeviceNotFoundException {
		Optional<Device> deviceOpt = deviceRepository.findById(Long.parseLong(id));
		if (!deviceOpt.isPresent()) {
			throw new DeviceNotFoundException("Device with id: " + id + " not found");
		}
		return deviceOpt.get();
	}

	public Device getDeviceByMacAddress(String macAddress) throws DeviceNotFoundException {
		Optional<List<Device>> deviceOpt = deviceRepository.findByMacAddress(macAddress);
		if (!deviceOpt.isPresent()) {
			throw new DeviceNotFoundException("Device with mac: " + macAddress + " not found");
		}
		return deviceOpt.get().get(0);
	}

}
