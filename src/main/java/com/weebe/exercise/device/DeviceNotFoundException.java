package com.weebe.exercise.device;

import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.http.HttpStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class DeviceNotFoundException extends Exception {

	public DeviceNotFoundException() {
	}

	public DeviceNotFoundException(String message) {
		super(message);
	}
}
