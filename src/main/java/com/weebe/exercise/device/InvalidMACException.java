package com.weebe.exercise.device;

import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.http.HttpStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class InvalidMACException extends Exception {

	public InvalidMACException() {
	}

	public InvalidMACException(String message) {
		super(message);
	}
}
