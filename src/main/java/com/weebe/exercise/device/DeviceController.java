package com.weebe.exercise.device;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DeviceController {

	@Autowired
	private DeviceService deviceService;

	/**
	 * Save a Device on device collection.
	 *
	 * @param Device to be saved.
	 * @throws InvalidMACException, BadTimestampException
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/device")
	public void addMenuItem(@RequestBody Device device) throws InvalidMACException, BadTimestampException {
		deviceService.addDevice(device);
	}

	/**
	 * List all saved devices.
	 *
	 * @return list of devices saved on device collection.
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/device")
	public List<Device> getDevices() {
		return deviceService.getAllDevices();
	}

	/**
	 * Get device details for specific id if found.
	 *
	 * @return list of devices saved on device collection.
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/device/id/{id}")
	public Device getDeviceById(@PathVariable("id") String id) throws DeviceNotFoundException {
		return deviceService.getDeviceById(id);
	}

	/**
	 * Get device details for specific mac address if found.
	 *
	 * @return list of devices saved on device collection.
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/device/mac/{mac}")
	public Device getDeviceByMacAddress(@PathVariable("mac") String macAddress) throws DeviceNotFoundException {
		return deviceService.getDeviceByMacAddress(macAddress);
	}
}
