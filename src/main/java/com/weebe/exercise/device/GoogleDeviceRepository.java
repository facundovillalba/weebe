package com.weebe.exercise.device;

import java.util.List;
import java.util.Optional;

import org.springframework.cloud.gcp.data.datastore.repository.DatastoreRepository;

public interface GoogleDeviceRepository extends DatastoreRepository<Device, Long> {

	Optional<List<Device>> findByMacAddress(String adress);

}
